#ifndef COPIERIMPL_H
#define COPIERIMPL_H

#include <QObject>
#include <QList>
class CopierImpl : public QObject
{
	Q_OBJECT

public:
	class Info
	{
	public:
		Info(QString sname, QString spath):
		  s_name(sname), s_path(spath){}
		QString s_name;
		QString s_path;
	};
	CopierImpl(QObject *parent);
	~CopierImpl();
	void copyContent(QString sconfigDir, QList<QString>& lsttargetDirs, QList<Info>& sconfigNames);
private:
	
};

#endif // COPIERIMPL_H
