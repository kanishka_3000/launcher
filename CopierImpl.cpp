#include "CopierImpl.h"
#include <QDir>
CopierImpl::CopierImpl(QObject *parent)
	: QObject(parent)
{

}

CopierImpl::~CopierImpl()
{

}

void CopierImpl::copyContent( QString sconfigDir, QList<QString>& lsttargetDirs , QList<Info>& lstconfigNames)
{
	QDir oconfigDir(sconfigDir);
	QFileInfoList oconfigs = oconfigDir.entryInfoList(QDir::Files);

	Q_FOREACH(QFileInfo ofileInfo, oconfigs)
	{
		QString sconfigPath = ofileInfo.absoluteFilePath();
		QString sname = ofileInfo.fileName();
		Info info(sname , sconfigPath);
		lstconfigNames.push_back(info);
	}
	if(lstconfigNames.size() == 0)
		return;

	Q_FOREACH(QString sdir, lsttargetDirs)
	{
		Q_FOREACH(Info oconfigFile , lstconfigNames)
		{
			QString stargetName = sdir + QDir::separator() + oconfigFile.s_name;
			QFile::copy(oconfigFile.s_path, stargetName);
		}
	}
}
