#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QtWidgets/QMainWindow>
#include "ui_Launcher.h"
#include <QFileDialog>
#include <QProcess>
#include <FinderImpl.h>
#include <iostream>
#include <QListWidgetItem>
#include <QSettings>
#include <QMessageBox>
#include <CopierImpl.h>
class Launcher : public QMainWindow
{
	Q_OBJECT

public:
	Launcher(QWidget *parent = 0);
	~Launcher();
	public slots:
		void onButton();
		void onConfigPathButton();
		void onCopyButton();
		void populate( QString path );

		void onListDoubleClicked(QListWidgetItem* item);
		void onError(QProcess::ProcessError error);
		void onStart();
		
private:

	bool getDirectoyPath(QLineEdit* lineEdit, QString ssettingsName, QString& spath);
	std::list<Finder::Info> lst_names;

	Ui::LauncherClass ui;
};

#endif // LAUNCHER_H
