#include "Launcher.h"

Launcher::Launcher(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.addtionalPane->setVisible(false);
	connect(ui.listExes, SIGNAL(itemDoubleClicked(QListWidgetItem * )),this, SLOT(onListDoubleClicked( QListWidgetItem*  )));
	QSettings osettings("launcher.dat");
	QVariant opath = osettings.value("saved_path");
	if(opath != QVariant())
	{
		QString spath = opath.toString();
		populate(spath);
	}
	QVariant oconfigPath = osettings.value("config_path");
	if(oconfigPath != QVariant())
	{
		QString spath = oconfigPath.toString();
		ui.txtconfigPath->setText(spath);
	}
}

Launcher::~Launcher()
{

}

void Launcher::onButton()
{
	QString path;
	if(getDirectoyPath(ui.lineEdit, "saved_path", path) == false)
		return;

	ui.listExes->clear();
	populate(path);

	
}
bool Launcher::getDirectoyPath(QLineEdit* lineEdit, QString ssettingsName, QString& spath)
{
	QString sRoot = QDir::homePath();
	if(lineEdit->text() != "")
	{
		sRoot = lineEdit->text();
	}
	QString path = QFileDialog::getExistingDirectory(this, "Select Root", sRoot);
	if(path.isEmpty())
	{
		QMessageBox::critical(this, "Invalid path", "Invalid path, Select a root directory");
		return false;
	}
	lineEdit->setText(path);
	QSettings osettings("launcher.dat");
	osettings.setValue(ssettingsName, path);

	spath = path;

	return true;
}
void Launcher::onListDoubleClicked( QListWidgetItem* item )
{
	
	QString sprocessName = item->data(25).toString();
	QString sdirName = item->data(26).toString();
	QDir::setCurrent(sdirName);
	QProcess* process = new QProcess();
	sprocessName.replace("/","//");
	sdirName.replace("/","//");
	sprocessName.append("\"");
	sprocessName.prepend("\"");
	sdirName.append("\"");
	sdirName.prepend("\"");
	connect(process, SIGNAL(QProcess::ProcessError ), this, SLOT(onError(QProcess::ProcessError)));
	//process->setWorkingDirectory(sdirName);
	
	int ival = process->startDetached(sprocessName);
	std::cout << "";
}

void Launcher::onError( QProcess::ProcessError error )
{
	std::cout << "";
}

void Launcher::onStart()
{

}

void Launcher::populate( QString path )
{
	ui.lineEdit->setText(path);
	
	FinderImpl finder(this);
	lst_names.clear();
	finder.getAllExes(qPrintable(path), lst_names);
	std::cout << lst_names.size() << std::endl;
	for(std::list<Finder::Info>::iterator itr = lst_names.begin(); itr != lst_names.end(); itr++)
	{
		QListWidgetItem* item = new QListWidgetItem(itr->s_name.c_str(),ui.listExes);
		item->setToolTip(itr->s_dir.c_str());
		item->setData(25, QString(itr->s_path.c_str()));
		item->setData(26, QString(itr->s_dir.c_str()));
		ui.listExes->addItem(item);
	}
}

void Launcher::onConfigPathButton()
{
	QString spath ;
	if(getDirectoyPath(ui.txtconfigPath, "config_path", spath) == false)
		return;
}

void Launcher::onCopyButton()
{
	if(lst_names.size() == 0)
		return;

	QString sconfigPath = ui.txtconfigPath->text();
	if(sconfigPath.isEmpty())
		return;

	QList<QString> lstName;
	for(std::list<Finder::Info>::iterator itr = lst_names.begin(); itr != lst_names.end(); itr++)
	{
		QString spath = itr->s_dir.c_str();
		lstName.push_back(spath);
	}
	QList<CopierImpl::Info> copiedName;
	CopierImpl copier(this);
	copier.copyContent(sconfigPath, lstName, copiedName);
	QString smsg = QString("%1 config files were copied, Note that files will not be overridden").arg(copiedName.size());
	QMessageBox::information(this, "Done", smsg);
}
