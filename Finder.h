#pragma once

#include <list>
#include <string>
class Finder
{
public:
	class Info
	{
	public:
		Info(std::string name, std::string path, std::string dir):
		  s_name(name), s_path(path), s_dir(dir){}
		std::string s_name;
		std::string s_path ;
		std::string s_dir;
	};
	virtual void getAllExes(std::string sRoot, std::list<Info>& lstNames) = 0;
};